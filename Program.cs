﻿using System;

namespace exercise_15
{
    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            Console.Clear();

            var  number = 0;
            Console.WriteLine("Can you enter a number");
            number = int.Parse (Console.ReadLine());


     


                        Console.WriteLine($"{number} X 1 = {number*1}");
                        Console.WriteLine($"{number} X 2 = {number*2}");
                        Console.WriteLine($"{number} X 3 = {number*3}");               
                        Console.WriteLine($"{number} X 4 = {number*4}");
                        Console.WriteLine($"{number} X 5 = {number*5}");
                        Console.WriteLine($"{number} X 6 = {number*6}");               
                        Console.WriteLine($"{number} X 7 = {number*7}"); 
                        Console.WriteLine($"{number} X 8 = {number*8}");
                        Console.WriteLine($"{number} X 9 = {number*9}");
                        Console.WriteLine($"{number} X 10 = {number*10}");
                        Console.WriteLine($"{number} X 11 = {number*11}");
                        Console.WriteLine($"{number} X 12 = {number*12}");


           
            
            
            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
        }
    }
}
